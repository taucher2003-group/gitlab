# frozen_string_literal: true

module Integrations
  module Instance
    class AmazonQ < Integration
      include Integrations::Base::AmazonQ
    end
  end
end
